import zad04.Student;

import java.io.*;


public class Main implements Serializable {
    private static final String filepath="C:\\Users\\Mateusz\\Dropbox\\Uczelnia\\PWJ\\2017\\8\\zad\\zad04\\object.obj";
    public static void main(String[] args) {
        Main objectIO = new Main();
        Student student = (Student) objectIO.ReadObjectFromFile(filepath);
        System.out.println(student);
    }

    public Object ReadObjectFromFile(String filepath) {
        try {
            FileInputStream myFile = new FileInputStream(filepath);
            ObjectInputStream myObject = new ObjectInputStream(myFile);
            Object obj = myObject.readObject();
            ObjectInputStream.GetField fields = myObject.readFields();
            ObjectStreamField[] fields1 = fields.getObjectStreamClass().getFields();
            //test
            myObject.close();
            return obj;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
}

